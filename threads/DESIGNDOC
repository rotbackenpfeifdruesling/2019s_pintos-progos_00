			+--------------------+
			|        CS 140      |
			| PROJECT 1: THREADS |
			|   DESIGN DOCUMENT  |
			+--------------------+
				
---- GROUP ----

>> Fill in the names and email addresses of your group members.

David Pernerstorfer <e1633068@student.tuwien.ac.at>

---- PRELIMINARIES ----

>> If you have any preliminary comments on your submission, notes for the
>> TAs, or extra credit, please give them here.

>> Please cite any offline or online sources you consulted while
>> preparing your submission, other than the Pintos documentation, course
>> text, lecture notes, and course staff.


			 PRIORITY SCHEDULING
			 ===================

---- DATA STRUCTURES ----

>> B1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

--------------------
struct thread
  {

    ...

    int priority;               /* actual priority of the thread */
    int priority_origin;        /* initial priority */
    struct thread *pri_donee;   /* in case of a locked lock, donee thread receives priority of this thread */
    struct list pri_donations;  /* priority donation this thread received */

    ...

  };

struct pri_donation
  {
    int priorty;
    struct lock *lock;
    struct list_elem elem;
  };

>> B2: Explain the data structure used to track priority donation.
>> Use ASCII art to diagram a nested donation.  (Alternately, submit a
>> .png file.)

---- ALGORITHMS ----

>> B3: How do you ensure that the highest priority thread waiting for
>> a lock, semaphore, or condition variable wakes up first?
by using a list ordered by priority

>> B4: Describe the sequence of events when a call to lock_acquire()
>> causes a priority donation.  How is nested donation handled?
* sema_try_down() -> if lock currently not acquired, the thread gets the lock -> end
* add lock to list of priorty donation in holder's thread
* update priorty of holder thread to priorty of current thread
* for nested donation:
* check holder is donor and update priority of donee recursively

>> B5: Describe the sequence of events when lock_release() is called
>> on a lock that a higher-priority thread is waiting for.
* update holder of lock (=NULL)
* delete priority donation from list
* update priority of current thread to highest priority in donation list or 
  to priority_origin if list is empty

---- SYNCHRONIZATION ----

>> B6: Describe a potential race in thread_set_priority() and explain
>> how your implementation avoids it.  Can you use a lock to avoid
>> this race?
During the execution of thread_set_priority(), a interrupt could 
create a threat with higher priority. If this new thread donates 
his priorty to the thread that was currently setting its own 
priorty, a race could occur.
In order to avoid a race, interrupts are disabled.

---- RATIONALE ----

>> B7: Why did you choose this design?  In what ways is it superior to
>> another design you considered?

			   SURVEY QUESTIONS
			   ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the course evaluations at the end of
the quarter.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?

>> Any other comments?
