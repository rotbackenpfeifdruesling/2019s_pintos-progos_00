			+--------------------+
			|  OS Development    |
			| PROJECT 0: INTRO   |
			|   DESIGN DOCUMENT  |
			+--------------------+
				
---- GROUP ----

>> Fill in the names and email addresses of your group members.

David Pernerstorfer <e1633068@student.tuwien.ac.at>

---- PRELIMINARIES ----

>> If you have any preliminary comments on your submission, notes for the
>> TAs, or extra credit, please give them here.

>> Please cite any offline or online sources you consulted while
>> preparing your submission, other than the Pintos documentation, course
>> text, lecture notes, and course staff.

			     ALARM CLOCK
			     ===========

---- DATA STRUCTURES ----

>> A1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

---------------
timer.c:

/* list of threads waiting for timer to end */
static struct list sleeping_thread_list;

---------------
thread.h:

struct thread
  {
    
    ...

    /* used if timer_sleep() (devices/timer.c) is called;
       stores wake up time of thread (unit: timer ticks) */
    int64_t timer_end;

    /* list element for list of threads waiting for a timer */
    struct list_elem timerelem;
  };

---- ALGORITHMS ----

>> A2: Briefly describe what happens in a call to timer_sleep(),
>> including the effects of the timer interrupt handler.

in timer_sleep() the wake up time for threads is calculated 
(= current ticks + timer ticks) and is stored in the thread struct. the 
current thread is added to the ordered (by wake up time) list of sleeping 
threads. the thread then gets blocked.

the timer interrupt handler iterates the ordered list of threads and wakes
up the threads when the timer is over. The thread is removed from the list
and gets unblocked.

>> A3: What steps are taken to minimize the amount of time spent in
>> the timer interrupt handler?

during every timer interrupt, the sleeping threads that have to be unblocked
must be found. therefore the timer_end value of the threads must be compared
with the current time. instead of iterating the list completely every time, 
the list is ordered by the timer_end and the iteration can be aborted 
early. (Usually the loop can be aborted within the first iteration if 
there are no threads to wake up.)

---- SYNCHRONIZATION ----

>> A4: How are race conditions avoided when multiple threads call
>> timer_sleep() simultaneously?

interrupts are disabled during timer_sleep().

>> A5: How are race conditions avoided when a timer interrupt occurs
>> during a call to timer_sleep()?

interrupts are disabled during timer_sleep().

---- RATIONALE ----

>> A6: Why did you choose this design?  In what ways is it superior to
>> another design you considered?

As pointed out in A3, the sleeping threads are stored in an ordered list to
minimize the amount of time spent in the timer interrupt handler.

Since the timer_sleep() function is only very short, i chose to disable the
interrupts for synchronization.


			   ARGUMENT PASSING
			   ================

---- DATA STRUCTURES ----

>> A1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

---------------
process.c

struct start_aux_data {

  ...

  /* parsed arguments passed to the user program */
  char **argv;
  /* number of arguments passed to the user program */
  int argc;
};


---- ALGORITHMS ----

>> A2: Briefly describe how you implemented argument parsing.  How do
>> you arrange for the elements of argv[] to be in the right order?
>> How do you avoid overflowing the stack page?

when the process_execute() function is called, a page for the arguments
and another page for the argv pointers is allocated. the command string
is then parsed and the number of arguments is counted. argv and argc are
passed to the function setup_stack() where the initial stack for the
user program is built up.

in setup_stack() a page for the user stack is allocated. the stack
pointer is initialized to PHYS_BASE. since the stack grows downward,
the stack pointer is always decremented before something is copied
onto the stack.
the arguments are copied one by one onto the stack, the order does not
matter. further the pointers to the arguments on the stack are temporarily
stored in temporary allocated page.
after pushing the argument strings to the stack, the stack pointer is
decremented by the number of arguments times size of a pointer and 
then the whole pointer array is copied to the stack. therefore
the argument pointers are in the correct order.

the maximum size for the initial stack is calculated as follows:
maximum command string size is 128 chars. in the worst case this 
is 64 single character arguments separated by spaces.
total stack size = 128 bytes for argument strings + 65 argument
pointers + another pointer to argv + argc (int) + fake return address = 400 bytes 
this leaves about 3700 bytes for the user program stack, what will be 
sufficient in most cases.

---- RATIONALE ----

>> A3: Why does Pintos implement strtok_r() but not strtok()?

strtok() uses a static global variable as save pointer what is unsafe
in threaded programs such as kernels because all threads have access 
to the same global variables. therefore synchronization would be 
necessary.

>> A4: In Pintos, the kernel separates commands into a executable name
>> and arguments.  In Unix-like systems, the shell does this
>> separation.  Identify at least two advantages of the Unix approach.

the kernel time is shorter because its not necessary to allocate another
page nor to parse and copy the arguments.
possible invalid inputs could be catched early, without switching into 
kernel mode.

			   SURVEY QUESTIONS
			   ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the course evaluations at the end of
the quarter.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?

>> Any other comments?
